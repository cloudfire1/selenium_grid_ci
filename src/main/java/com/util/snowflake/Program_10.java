package com.util.snowflake;

import java.util.ArrayList;
import java.util.List;

// find the pairs in array of Integers whose sum is equal to a given number in java????
public class Program_10 {
	
	
	public static List<String> findPairsInArray(int [] arr_2, int sum) {
		
		
		List<String> pairs = new ArrayList<String>();
		
		
		for (int i = 0; i < arr_2.length; i++) {
			int first = arr_2[i];
			
			for (int j = i+1; j < arr_2.length; j++) {
				int second = arr_2[j];
				
				
				if ((first+second)==sum) {
				//	System.out.printf("(%d,%d) %n",first,second);
					
					pairs.add("("+String.valueOf(first)+","+String.valueOf(second)+")");
				}
			}
			
			
		}
		return pairs;
		
		
	}
	
	
	
	
	
	
// modulo %
	public static void main(String[] args) {
		
		int arr[] = {1,2,3,4,5,6,7,8,9};
		
	int sum =10;
	List<String>  pairs=	findPairsInArray(arr,sum);
	
	for (String pair : pairs) {
		System.out.printf("%s   %n",pair);
	}

	}

}
